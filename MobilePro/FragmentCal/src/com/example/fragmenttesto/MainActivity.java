package com.example.fragmenttesto;

/**
Program cal by Interaction with main and fragments.
Author : Choi Min Jun.
Email Address: choiminjun0720@gmail.com
HW3-a
Last Changed : 14/03/2014.
*/

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import com.example.fragmentcal.R;

 

public class MainActivity extends FragmentActivity implements HeadFragment.CustomOnClickListener{
     
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        
        FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = getSupportFragmentManager()
				.beginTransaction();
		// ---get the current display info---
		WindowManager wm = getWindowManager();
		Display d = wm.getDefaultDisplay();
		if (d.getWidth() > d.getHeight()) {
			// ---landscape mode---
			setContentView(R.layout.activity_main_width);
		} else {
			setContentView(R.layout.activity_main);
		}
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
        
    }
 
    /**
     *  Implemetation of HeadFragment.CustomOnClickListener
     *  이것을 사용함으로써 두 Fragment간에 직접 통신하지 않고 MainActivty 를 통하여 통신한다.
     */
    @Override
    public void onClicked(int id) {
    	/**
    	 * This is for interaction with tail,head Fragments.
    	 */
        TailFragment tailFragment = (TailFragment)getFragmentManager().findFragmentById(R.id.tail_fragment);
        HeadFragment headFragment = (HeadFragment)getFragmentManager().findFragmentById(R.id.head_fragment);
        
        String display_result = headFragment.getResult();	//by object, program can use method which gets value of functions.
        
        tailFragment.setResult(display_result);	//send result to tailFragment and using method.
    }
}
