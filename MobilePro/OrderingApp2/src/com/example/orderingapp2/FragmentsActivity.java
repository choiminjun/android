package com.example.orderingapp2;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.WindowManager;
import android.support.v4.app.FragmentManager;
import android.app.Activity;
import android.view.Menu;
public class FragmentsActivity extends FragmentActivity {
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.main);
	
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		//---get the current display info---
		WindowManager wm = getWindowManager(); 
		Display d = wm.getDefaultDisplay();
		if (d.getWidth() > d.getHeight())
		{
		//---landscape mode---
			setContentView(R.layout.main2);
		//Fragment1 fragment1 = new Fragment1(); // android.R.id.content refers to the content // view of the activity fragmentTransaction.replace(
		//fragmentTransaction.replace( android.R.id.content, fragment1);
		}
		else
		{
			setContentView(R.layout.main);
			//Fragment2 fragment2 = new Fragment2(); 
			//fragmentTransaction.replace( android.R.id.content, fragment2);
		}
		fragmentTransaction.addToBackStack(null); 
		fragmentTransaction.commit();
	
	}
}
