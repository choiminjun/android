/**
Program SimpleTipCalculator.
Author : Choi Min Jun.
Email Address: choiminjun0720@gmail.com
Hw 2-b.
Last Changed : 19/03/2014.
 */
package com.example.simpletipcalculator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

class AExceptionOne extends Exception {
}

class AExceptionTwo extends Exception {
}

class AExceptionThree extends Exception {
}

public class Fragment2 extends Fragment {

	TextView Name;
	EditText Price_Input;
	RadioButton rb1, rb2, rb3;
	EditText rb3_Input; // which gets other rate of tax.
	TextView result; // result that show adding TAX and PRICE.
	Button btn;

	/**
	 * this is for exception handling case nonvalue or negative number. handles
	 * exception by method which is named checking and judging what exceptions
	 * they have.
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment2, container, false);
	}

/**
	public void onStart(){
		super.onStart();
		// Name = (TextView) findViewById(R.id.LabelName);
		Price_Input = (EditText)getActivity().findViewById(R.id.value);
		rb1 = (RadioButton)getActivity().findViewById(R.id.radio01);
		//rb1.setOnCheckedChangeListener(this);
		rb2 = (RadioButton)getActivity().findViewById(R.id.radio02);
		//rb2.setOnCheckedChangeListener(this);
		rb3 = (RadioButton)getActivity().findViewById(R.id.radio03);
		//rb3.setOnCheckedChangeListener(this);
		btn = (Button)getActivity().findViewById(R.id.calBtn);
		rb3_Input = (EditText)getActivity().findViewById(R.id.radio03_Text);
		result = (TextView)getActivity().findViewById(R.id.result);
		btn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				try {
					display();
				} catch (AExceptionTwo e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (AExceptionThree e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	
	}

	static void checking(int i, int price) throws AExceptionOne, AExceptionTwo,
			AExceptionThree {
		if (i < 0)
			throw new AExceptionOne();
		else if (i == 0)
			throw new AExceptionTwo();
		else if (price <= 0)
			throw new AExceptionThree();
	}

	public void display() throws AExceptionTwo, NumberFormatException,
			AExceptionThree {
		String price = Price_Input.getText().toString();
		String rate = "";
		String display = null;
		int tip_rate;
		double tip;
		double total; // price + tax.

		try {

			if (rb1.isChecked())
				rate = rb1.getText().toString().substring(0, 2); // get rate.
			if (rb2.isChecked())
				rate = rb2.getText().toString().substring(0, 2);
			if (rb3.isChecked()) {
				rate = rb3_Input.getText().toString();
			}
			checking(Integer.parseInt(rate), Integer.parseInt(price));
			tip_rate = Integer.parseInt(rate); // convert to int because
												// EditText can read only
												// String.

			tip = Integer.parseInt(price) * (double) (tip_rate / 100.0); // calculating
																			// tip.
			total = tip + Integer.parseInt(price);
			display = "Tip : " + Double.toString(tip) + "\nTotal : "
					+ Double.toString(total);

			result.setText(display);
			//Toast.makeText(this, display, Toast.LENGTH_LONG).show();

		} catch (AExceptionOne o) {
			result.setText("You Can not negative number for rate!!");
		} catch (AExceptionTwo t) {
			result.setText("You Can not put 0 for rate");
		} catch (AExceptionThree s) {
			result.setText("Price must bigger than 0!");

		}
	}

	*/
}
