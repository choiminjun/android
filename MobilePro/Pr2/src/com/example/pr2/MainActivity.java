package com.example.pr2;

import android.app.Activity;
import android.os.Bundle;
import android.app.ListActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ListActivity {
	String[] presidents = { "Eisenhower", "Kennedy", "Johnson", "Nixon",
			"Ford", "Carter", "Reagan", "Bush", "Clinton", "Bush", "Obama" };

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, presidents));
	}

	public void onListItemClick(ListView parent, View v, int position, long id) {
		Toast.makeText(this, "you have selected " + presidents[position],
				Toast.LENGTH_SHORT).show();
	}
}