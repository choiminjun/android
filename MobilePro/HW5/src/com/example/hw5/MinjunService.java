package com.example.hw5;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class MinjunService extends Service implements LocationListener,
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {
	private LocationManager mLocationManager;
	private LocationListener mLocationListener;
	private LocationRequest mLocationRequest;
	private LocationClient mLocationClient;
	private String callerNumber;

	public MinjunService() {
		;
	}

	@Override
	public void onCreate() {
		/**
		 * intervalTime is 5 minutes
		 * numUpdates is 10 times
		 * setExpirationTime by intervalTime * numUpdates.
		 */
		int intervalTime = 300000;
		int numUpdates = 10;
		mLocationRequest = LocationRequest.create();

		mLocationRequest.setInterval(intervalTime);		//set intervalTime
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setFastestInterval(intervalTime);
		mLocationRequest.setNumUpdates(numUpdates);		//set how numtimes to set
		//mLocationRequest.setExpirationTime(intervalTime * numUpdates);		//set expirationTime.

		mLocationClient = new LocationClient(getApplicationContext(), this,
				this);
		mLocationClient.connect();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		callerNumber = intent.getStringExtra("callerNumber");
		System.out.println(callerNumber);
		int start = Service.START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	/**
	 * program can receive GPS reading only when this method is called.So request
	 * for location updates from this method rather than onStart()
	 */
	@Override
	public void onConnected(Bundle arg0) {
		Log.i("info", "Location Client is Connected");
		mLocationClient.requestLocationUpdates(mLocationRequest, this);
		Log.i("info", "Service Connect status :: " + isServicesConnected());
	}

	@Override
	public void onDisconnected() {
		Log.i("info", "Location Client is Disconnected");
	}

	/*
	 * Overrriden method of interface LocationListener called when location of
	 * gps device is changed. Location Object is received as a parameter. This
	 * method is called when location of GPS device is changed
	 */
	@Override
	public void onLocationChanged(Location location) {
		double latitude = location.getLatitude();
		double longitude = location.getLongitude();
		SmsManager smsManager = SmsManager.getDefault();
		smsManager.sendTextMessage(callerNumber, null, "LOCATION," + latitude
				+ "," + longitude, null, null);

		Log.i("info", "Latitude :: " + latitude);
		Log.i("info", "Longitude :: " + longitude);
		/**
		 * Send location and longitude to caller.
		 */
	}

	private boolean isServicesConnected() {
		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(MinjunService.this);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Called when Sevice running in background is stopped. Remove location
	 * upadate to stop receiving gps reading
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.i("info", "Service is destroyed");
		mLocationClient.removeLocationUpdates(this);
		super.onDestroy();
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		;
	}

}