package com.example.quiz0410choi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	Button btn1, btn2, btn3;
	TextView txt1, txt2;
	Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btn1 = (Button) findViewById(R.id.button1);
		btn1.setOnClickListener(this);
		btn2 = (Button) findViewById(R.id.button2);
		btn2.setOnClickListener(this);
		btn3 = (Button) findViewById(R.id.button3);
		btn3.setOnClickListener(this);
		txt1 = (TextView) findViewById(R.id.textView1);
		txt2 = (TextView) findViewById(R.id.textView2);
	}

	@Override
	public void onClick(View v) {
		if ((v.getId() == btn1.getId()) || (v.getId() == btn2.getId())) {
			if (v.getId() == btn1.getId())
				Toast.makeText(this, btn1.getText().toString(),
						Toast.LENGTH_LONG).show();
			// btnClicked(btn1.getId()); //call method.
			else
				Toast.makeText(this, btn2.getText().toString(),
						Toast.LENGTH_LONG).show();
			// btnClicked(btn2.getId()); //call method.
		}

		if (v.getId() == btn3.getId()) {
			intent = new Intent(this, MinjunsActivity.class);
			startActivityForResult(intent, 101);
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if ((requestCode == 101) && (resultCode == Activity.RESULT_OK)) {
			Bundle mydata = new Bundle();
			mydata = data.getExtras();
			String tel = data.getStringExtra("tel");
			String number = data.getStringExtra("number");
			txt1.setText("tel number is " + number);
			txt2.setText("Service provide " + tel);
			// finish();
		}
	}

	public void btnClicked(int temp) {
		if (temp == btn1.getId())
			Toast.makeText(this, txt1.getText(), Toast.LENGTH_LONG);
		else
			Toast.makeText(this, txt2.getText(), Toast.LENGTH_LONG);
	}
}
