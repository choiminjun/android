package com.example.sharedpreference;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements OnClickListener {

	Button submit, exit;
	String username, password;
	EditText userinput, passinput;
	SharedPreferences sh_Pref;
	Editor toEdit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getInit();
		applySharedPreferences();
	}

	public void getInit() {
		submit = (Button) findViewById(R.id.submit);
		exit = (Button) findViewById(R.id.exit);
		userinput = (EditText) findViewById(R.id.userinput);
		passinput = (EditText) findViewById(R.id.passinput);
		submit.setOnClickListener(this);
		exit.setOnClickListener(this);
	}

	public void sharedPrefernces() {
		sh_Pref = getSharedPreferences("Login Credentials", MODE_PRIVATE);
		toEdit = sh_Pref.edit();
		toEdit.putString("Username", username);
		toEdit.putString("Password", password);
		toEdit.commit(); // 이것이 선언됨으로써 데이터를 안으로 넣
	}

	@Override
	public void onClick(View currentButton) {
		switch (currentButton.getId()) {
		case R.id.submit:
			username = userinput.getText().toString();
			password = passinput.getText().toString();
			sharedPrefernces();
			Toast.makeText(this, "Details are saved", 20).show();
			break;
		case R.id.exit:
			finish();
		}
	}

	public void applySharedPreferences() {
		sh_Pref = getSharedPreferences("Login Credentials", MODE_PRIVATE);
		if (sh_Pref != null && sh_Pref.contains("Username")) {
			String a = sh_Pref.getString("Username", "defValue");
			userinput.setText(a);
			String b = sh_Pref.getString("Password", "defValue");
			passinput.setText(b);
			// int p = sh_Pref.getInt("password", 0);
			// SharedPreferences.Editor editor = sh_Pref.edit();
			// String a = sh_Pref.getString("Username", "defValue");
			// int p = sh_Pref.getInt("Password", 0);
			// Toast.makeText(this, a, Toast.LENGTH_LONG).show();
			// sharedPrefernces();
		}
	}
}
