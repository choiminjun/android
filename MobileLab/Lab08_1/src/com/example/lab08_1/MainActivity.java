package com.example.lab08_1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void onClickWebBrowser(View view) {
		Intent i = new Intent(android.content.Intent.ACTION_VIEW,
				Uri.parse("http://www.amazon.com"));

		startActivity(i);
	}

	public void onClickMakeCalls(View view) {
		Intent i = new Intent(android.content.Intent.ACTION_DIAL,
				Uri.parse("tel:+0317505114"));
		startActivity(i);
	}

	public void onClickShowMap(View view) {
		Intent i = new Intent(android.content.Intent.ACTION_VIEW,
				Uri.parse("geo:37.450673,127.128468"));
		startActivity(i);
	}

	public void onClickLaunchMyBrowser(View view) {
		//Intent i = new Intent(this,MyBrowserActivity.class);
		Intent i = new Intent("com.example.lab08_1.MyBrowser");
		i.setData(Uri.parse("http://sw.gachon.ac.kr"));
		//i.addCategory("com.example.MyApp");		//This is important.
		startActivity(i);
	}
}
