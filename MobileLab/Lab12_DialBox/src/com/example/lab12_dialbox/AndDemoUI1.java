package com.example.lab12_dialbox;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AndDemoUI1 extends Activity {
	Button btnGo;
	EditText txtMsg;
	String msg;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_and_demo_ui1);
		txtMsg = (EditText) findViewById(R.id.txtMsg);
		btnGo = (Button) findViewById(R.id.btnGo);
		btnGo.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				AlertDialog dialBox = createDialogBox();
				dialBox.show();
				txtMsg.setText("I am here!");
			}
		});
	}// onCreate

	private AlertDialog createDialogBox() {
		AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
				.setTitle("Terminator")
				.setMessage("Are you sure that you want to quit?")
				.setIcon(R.drawable.ic_launcher)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// TODO Auto-generated method stub
								msg = "YES " + Integer.toString(whichButton);
								txtMsg.setText(msg);
							}
                        })
				.setNeutralButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// TODO Auto-generated method stub
								msg = "CANCEL " + Integer.toString(whichButton);
								txtMsg.setText(msg);
							}
						})
				.setNegativeButton("NO", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						// TODO Auto-generated method stub
						msg = "NO " + Integer.toString(whichButton);
						txtMsg.setText(msg);
					}
				}).create();
		return myQuittingDialogBox;
	}
}
