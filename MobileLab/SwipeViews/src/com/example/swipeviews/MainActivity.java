package com.example.swipeviews;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;

public class MainActivity extends Activity {
@Override
public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);
// Create and set adapter
CustomPagerAdapter adapter = new CustomPagerAdapter();
ViewPager myPager = (ViewPager) findViewById(R.id.c);
myPager.setAdapter(adapter);
myPager.setCurrentItem(0);
}
@Override
public boolean onCreateOptionsMenu(Menu menu) { 
	getMenuInflater().inflate(R.menu.main, menu);
return true;
}
}
}
