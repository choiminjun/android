package com.example.lab14_thread;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Runnable myRunnable1 = new MyRunnableClass();
		Thread t1 = new Thread(myRunnable1);
		t1.start();
		MyThread t = new MyThread();
		t.start();
	}// onCreate
}
