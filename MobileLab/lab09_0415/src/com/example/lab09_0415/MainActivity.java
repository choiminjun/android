package com.example.lab09_0415;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends ActionBarActivity {
	private static final int MENU_ONE = 0;
	private static final int MENU_TWO = 1;
	private static final int MENU_THREE = 2;
	private static final int MENU_FOUR = 3;
	private static final int MENU_FIVE = 4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_ONE, Menu.NONE, "One").setIcon(
				android.R.drawable.ic_menu_add);
		menu.add(0, MENU_THREE, Menu.NONE, "Three").setIcon(
				android.R.drawable.ic_menu_call);
		menu.add(0, MENU_FOUR, Menu.NONE, "Four").setIcon(
				android.R.drawable.ic_menu_camera);
		menu.add(0, MENU_FIVE, Menu.NONE, "Five")
				.setIcon(android.R.drawable.ic_menu_close_clear_cancel)
				.setCheckable(true);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/**
		 * String text = " ID " + item.getItemId() + " title " +
		 * item.getTitle();
		 * 
		 * Toast.makeText(this, text, 1).show(); return true;
		 */

		if (item.getTitle().equals("Five")) {
			Toast.makeText(this, "Menu Selected : " + item.getItemId(),
					Toast.LENGTH_SHORT).show();
			item.setChecked(!item.isCheckable());
			return true;
		}
		
		switch (item.getItemId()) {
		case MENU_ONE:
		case MENU_TWO:
		case MENU_THREE:
		case MENU_FOUR:
			Toast.makeText(this, "Menu Selected : " + item.getItemId(),
					Toast.LENGTH_SHORT).show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
