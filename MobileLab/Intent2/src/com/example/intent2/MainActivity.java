package com.example.intent2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends ActionBarActivity implements OnClickListener {
	Button btn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btn = (Button)findViewById(R.id.Btn);
		btn.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		Intent myintent = new Intent(getApplicationContext(),Activity2.class);
		startActivity(myintent);
	}

}
