package com.example.lab17_web3;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebView4 extends Activity {
	WebView browser;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view4);
		// connect browser to local html file showing map
		browser = (WebView) findViewById(R.id.webview);
		browser.getSettings().setJavaScriptEnabled(true);
		browser.loadUrl("file:///android_asset/wepview_map.html");
	}
}
