package com.example.listviewtest;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements AdapterView.OnItemClickListener {

	ArrayList<String> arGeneral = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	
		arGeneral.add("김유신");
		arGeneral.add("이순신");
		arGeneral.add("강감찬");
		arGeneral.add("을지문덕");
		
		ArrayAdapter<String> Adapter;
		Adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arGeneral);
		
		ListView list = (ListView)findViewById(R.id.list);
		
		list.setOnItemClickListener(this);
		list.setAdapter(Adapter);
		//리스트 액티비티
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Toast.makeText(this, arGeneral.get(position), Toast.LENGTH_LONG).show();
	}
}
