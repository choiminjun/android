package com.example.lab1_practice;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Activity2 extends Activity implements OnClickListener{
	

	EditText dataReceived;
	Button btnDone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity2);
		dataReceived = (EditText) findViewById(R.id.etDataReceived);
		btnDone = (Button) findViewById(R.id.btnDone);
		btnDone.setOnClickListener(this);
		// pick call made to Activity2 via Intent
		Intent myLocalIntent = getIntent();
		// look into the bundle sent to Activity2 for data items
		Bundle myBundle = myLocalIntent.getExtras();
		Double v1 = myBundle.getDouble("val1");
		Double v2 = myBundle.getDouble("val2");
		// operate on the input data
		Double vResult = v1 + v2;
		// for illustration purposes. show data received & result
		dataReceived.setText("Data received is \n" + "val1= " + v1 + "\nval2= "
				+ v2 + "\n\nresult= " + vResult);
		// add to the bundle the computed result
		myBundle.putDouble("vresult", vResult);
		// attach updated bumble to invoking intent
		myLocalIntent.putExtras(myBundle);
		// return sending an OK signal to calling activity
		setResult(Activity.RESULT_OK, myLocalIntent);
	}// onCreate

	@Override
	public void onClick(View v) {
		// close current screen ‐ terminate Activity2
		finish();
	}// onClick

	/**
	TextView value1,value2,showResult;
	Button btn;
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity2);
		value1 = (TextView)findViewById(R.id.showVal1);
		value2 = (TextView)findViewById(R.id.showVal2);
		showResult = (TextView)findViewById(R.id.showResult);
		btn = (Button)findViewById(R.id.finishBtn);
		Intent myLocalIntent = getIntent();
		btn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				finish();
			}
			
		});
		Bundle mydata = myLocalIntent.getExtras();
		Double v1 = mydata.getDouble("val1");
		Double v2 = mydata.getDouble("val2");
		
		value1.setText("val1 is " + v1);
		value2.setText("val2 is " + v2);
		
		Double result = v1+ v2;
		showResult.setText("Result is " + result);
		mydata.putDouble("result", result);
		myLocalIntent.putExtras(mydata);
		setResult(Activity.RESULT_OK,myLocalIntent);
	}
	*/
}
