package com.example.lab0410;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		// Inflate the menu; this adds items to the action bar if it is present.
		CreateMenu(menu);
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}

	private boolean MenuChoice(MenuItem item) {

		switch (item.getItemId()) {
		case 0:
			Toast.makeText(this, "You clicked on Item 1", Toast.LENGTH_LONG)
					.show();
			return true;
		case 1:
			Toast.makeText(this, "You clicked on Item 2", Toast.LENGTH_LONG)
					.show();
			return true;
		case 2:
			Toast.makeText(this, "You clicked on Item 3", Toast.LENGTH_LONG)
					.show();
			return true;
		}
		return false;
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	private void CreateMenu(Menu menu) {
		MenuItem mnu1 = menu.add(0, 0, 0, "Item 1");
		mnu1.setIcon(R.drawable.ic_launcher);
		mnu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		MenuItem mnu2 = menu.add(0, 1, 1, "Item 2");
		mnu2.setIcon(R.drawable.ic_launcher);
		mnu2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		MenuItem mnu3 = menu.add(0, 2, 2, "Item 3");
		mnu3.setIcon(R.drawable.ic_launcher);
		mnu3.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}

}
