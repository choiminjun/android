package com.example.lab13_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class MyDrawing extends View {
	private Path mPath;
	private Paint mPaint;
	Point mPoint;

	private void init() {
		mPath = new Path();
		mPaint = new Paint();
		mPaint.setDither(true);
		mPaint.setColor(0xFFFF00FF);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(3);
		mPoint = new Point();
	}

	public MyDrawing(Context c) {
		super(c);
		init();
	}

	public MyDrawing(Context c, AttributeSet a) {
		super(c, a);
		init();
	}

	protected void onDraw(Canvas canvas) {
		canvas.drawPath(mPath, mPaint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float eventX = event.getX();
		float eventY = event.getY();
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			// mPath.reset();
			mPath.moveTo(eventX, eventY);
			//return true;
			break;
		case MotionEvent.ACTION_MOVE:
			mPath.quadTo(eventX, eventY, (mPoint.x + eventX) / 2,
					(mPoint.y + eventY) / 2);
			break;
		case MotionEvent.ACTION_UP:
			mPath.lineTo(eventX, eventY);
			break;
		}
		mPoint.x = (int) eventX;
		mPoint.y = (int) eventY;
		invalidate();
		return true;
	}
} // MyDrawing

