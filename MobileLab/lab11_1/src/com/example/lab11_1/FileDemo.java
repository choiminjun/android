package com.example.lab11_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FileDemo extends Activity {
	private final static String NOTES = "notes.txt";
	private EditText txtUIData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		txtUIData = (EditText) findViewById(R.id.txtUIData);
		Button btn = (Button) findViewById(R.id.close);
		btn.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
	}

	public void onPause() {
		super.onPause();
		try {
			OutputStreamWriter out = new OutputStreamWriter(openFileOutput(
					NOTES, 0));
			out.write(txtUIData.getText().toString());
			out.close();
		} catch (Throwable t) {
			Toast.makeText(this, "Exceptoin: " + t.toString(), 2000).show();
		}
	}

	public void onResume() {
		super.onResume();
		try {
			/**
			 * important.
			 */
			InputStream in = openFileInput(NOTES);
			if (in != null) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(in));
				String str = "";
				StringBuffer buf = new StringBuffer();
				while ((str = reader.readLine()) != null) {
					buf.append(str + "\n");
				}
				in.close();
				txtUIData.setText(buf.toString());
			}// if
		} catch (java.io.FileNotFoundException e) {
			// that's OK, we probably haven't created it yet
		} catch (Throwable t) {
			Toast.makeText(this, "Exception: " + t.toString(), 2000).show();
		}
	}
	/**
	 * public void PlayWithRawFiles() throws IOException { String str="";
	 * StringBuffer buf = new StringBuffer(); int fileResourceId =
	 * R.raw.my_text_file; InputStream is =
	 * this.getResources().openRawResource(fileResourceId); BufferedReader
	 * reader = new BufferedReader(new InputStreamReader(is)); if (is!=null) {
	 * while ((str = reader.readLine()) != null) { buf.append(str + "\n" ); } }
	 * is.close(); txtMsg.setText( buf.toString() ); }// PlayWithRawFiles } //
	 * File1Resources
	 */
}
