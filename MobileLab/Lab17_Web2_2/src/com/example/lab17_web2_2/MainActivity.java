package com.example.lab17_web2_2;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

public class MainActivity extends Activity {
	WebView browser;
	MyLocater locater = new MyLocater();
	Location mostRecentLocation;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// get a location fix (lat, lon)
		mostRecentLocation = fakeGetLocation();
		// set up the webview to show location results
		browser = (WebView) findViewById(R.id.webview);
		browser.getSettings().setJavaScriptEnabled(true);
		browser.addJavascriptInterface(locater, "locater");
		browser.loadUrl("file:///android_asset/my_local_webpage1.html");
	}

	private Location fakeGetLocation() {
		// faking the obtaining of a location object (discussed later!)
		Location fake = new Location("fake");
		fake.setLatitude(9.938038);
		fake.setLongitude(-84.054430);
		return fake;
	}

	public class MyLocater {
		private String commonData = "XYZ";
		@JavascriptInterface
		public double getLatitude() {
			if (mostRecentLocation == null)
				return (0);
			else
				return mostRecentLocation.getLatitude();
		}
		@JavascriptInterface
		public double getLongitude() {
			if (mostRecentLocation == null)
				return (0);
			else
				return mostRecentLocation.getLongitude();
		}
		@JavascriptInterface
		public void htmlPassing2Android(String dataFromHtml) {
			Toast.makeText(getApplicationContext(), "1\n" + commonData, 1)
					.show();
			commonData = dataFromHtml;
			Toast.makeText(getApplicationContext(), "2\n" + commonData, 1)
					.show();
		}

		@JavascriptInterface
		public String getCommonData() {
			return commonData;
		}

		@JavascriptInterface
		public void setCommonData(String actualData) {
			commonData = actualData;
		}

	}
}