package com.example.lab093_0415;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		Button mBtn;
		EditText mEdit;

		public PlaceholderFragment() {
			
		}
		public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState){
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			
			mBtn = (Button)rootView.findViewById(R.id.button);
			registerForContextMenu(mBtn);
			mEdit = (EditText)rootView.findViewById(R.id.edittext);
			registerForContextMenu(mEdit);
			return container;
			
		}
	
		public void onCreateContextMenu(ContextMenu menu, View v,
				ContextMenu.ContextMenuInfo menuInfo) {
			MenuInflater inflater = getActivity().getMenuInflater();
			inflater.inflate(R.layout.fragment_main, menu);
			//super.onCreateContextMenu(menu, v, menuInfo);
			if (v.getId() == mBtn.getId()) {
				menu.setHeaderTitle("Button Menu");
				menu.add(0, 1, 0, "Red");
				menu.add(0, 2, 0, "Green");
				menu.add(0, 3, 0, "Blue");
			}
			if (v == mEdit) {
				menu.add(0, 4, 0, "Translate");
				menu.add(0, 5, 0, "Recognition");
			}
		}
		public boolean onContextItemSelected(MenuItem item) {
			switch (item.getItemId()) {
			case 1:
				mBtn.setTextColor(Color.RED);
				return true;
			case 2:
				mBtn.setTextColor(Color.GREEN);
				return true;
			case 3:
				mBtn.setTextColor(Color.BLUE);
				return true;
			case 4:
				Toast.makeText(this.getActivity(), "Translation",
						Toast.LENGTH_SHORT).show();
				return true;
			case 5:
				Toast.makeText(this.getActivity(), "Recognition!",
						Toast.LENGTH_SHORT).show();
				return true;
			}
			return super.onContextItemSelected(item);
		}
	}

}
