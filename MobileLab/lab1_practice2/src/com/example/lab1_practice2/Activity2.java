package com.example.lab1_practice2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Activity2 extends Activity implements OnClickListener {
	EditText data1, data2;
	Button btn;
	Intent intent2;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity2);

		data1 = (EditText) findViewById(R.id.EditText01);
		data2 = (EditText) findViewById(R.id.EditText02);
		btn = (Button) findViewById(R.id.btnDone);
		btn.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		try {

			Double v1 = Double.parseDouble(data1.getText().toString());
			Double v2 = Double.parseDouble(data2.getText().toString());
			Bundle mydata = new Bundle();
			mydata.putDouble("val1", v1);
			mydata.putDouble("val2", v2);
			intent2 = new Intent();
			intent2.getExtras();
			setResult(Activity.RESULT_OK, intent2);
			finish();
		} catch (Exception e) {
			;
		}
	}
}
